<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProjectTechnology;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(ProjectTechnology::class, function (Faker $faker) {
    return [
        'project_id' => $faker->numberBetween($min = 1, $max = 30),
        'technology_id' => $faker->numberBetween($min = 1, $max = 10),
    ];
});
