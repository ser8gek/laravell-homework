@extends('adminlte::page')

@section('title', 'ADMIN')

@section('content_header')
    <h1 class="m-0 text-dark">PROJECTS</h1>
    <!-- Default box -->
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th style="width: 1%">
                        #
                    </th>
                    <th style="width: 15%">
                        Project Name
                    </th>
                    <th style="width: 20%">
                        Project Info
                    </th>
                    <th style="width: 5%">
                        Start Project Date
                    </th>
                    <th style="width: 5%">
                        Finish Project Date
                    </th>
                    <th style="width: 3%">
                        Developers On Project
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $project)
                    <tr>
                        <td>{{$project->id}}</td>
                        <td> {{$project->project_name}}</td>
                        <td>{{$project->project_info}}</td>
                        <td>{{$project->start_date}} </td>
                        <td>{{$project->finish_date}}</td>
                        <td>{{$project->developers_on_project}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@stop
