@extends('layouts.developer')

@section('content')
    <div class="container">
        <div class="container h-auto">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="col-lg-10 align-self-end">
                    <h1 class="text-uppercase text-black font-weight-bold">Project details</h1>
                    <hr class="divider my-4"/>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <form method="POST" action=()>
                @csrf
                @foreach($projects as $project)
                    @if(request()->id == $project->id)
                        <div class="card" style="width: 18rem;">
                            <div class="card-header">
                                DETAILS:
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">id: {{ $project->id }}</li>
                                <li class="list-group-item">project name: {{ $project->project_name }}</li>
                                <li class="list-group-item">project info: {{ $project->project_info }}</li>
                                <li class="list-group-item">start date: {{ $project->start_date }}</li>
                                <li class="list-group-item">finish date: {{ $project->finish_date }}</li>
                                <li class="list-group-item">developers on
                                    project: {{ $project->developers_on_project }}</li>
                            </ul>
                        </div>
                    @endif
                @endforeach
                <div class="card" style="width: 18rem;">
                    <div class="card-header">
                        technology:
                    </div>
                    @foreach($projects_technologies as $project_technology)
                        @if(request()->id == $project_technology->project_id)
                            @foreach($technologies as $technology)
                                @if($technology->id==$project_technology->technology_id)
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">{{ $technology->technology_name }}</li>
                                    </ul>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </div>
                <div class="col">
                    <a class="btn btn-secondary float-right" href="{{ url()->previous() }}">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection



