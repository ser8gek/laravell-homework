@extends('layouts.developer')

@section('content')
    <div class="container">
        <div class="container h-auto">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="col-lg-10 align-self-end">
                    <h1 class="text-uppercase text-black font-weight-bold">developer projects</h1>
                    <hr class="divider my-4"/>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <form method="POST" action={{route('developer.project')}}>
                @csrf
                @foreach($user_projects as $user_project)
                    @if(Auth::user()->id == $user_project->user_id)
                        @foreach($projects as $project)
                            @if($project->id == $user_project->project_id)
                                <div class="d-flex flex-column">
                                    <div class="d-flex justify-content-start">
                                        <p class="col col-md-12 btn-outline-dark">
                                            Project name: {{$project->project_name}}
                                        </p>
                                        <div class=btn>
                                            <input type="submit" class="btn btn-secondary "
                                                   name="id" value="{{$project->id}}">
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endforeach
                @if(empty($user_project->project[0]))
                    <div class="row mb-2">
                        <h1 class="text-uppercase text-black font-weight-bold"> Sorry,
                            but you have no working projects yet</h1>
                    </div>
                @endif
            </form>
        </div>
    </div>
@endsection


