@extends('layouts.developer')

@section('content')
    <div class="container bootstrap snippets bootdey">
        <h1 class="text-primary"><span class="glyphicon glyphicon-user"></span>My Profile</h1>
        <hr>
        <div class="row">
            <!-- left column -->
            <div class="col-md-3">
                <div class="text-center">
                    <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
                    <h6>Upload a different photo...</h6>

                    <input type="file" class="form-control">
                </div>
            </div>

            <!-- edit form column -->
            <div class="col-md-9 personal-info">
                <h3>Personal info</h3>
                <form class="form-horizontal" method="PUT" role="form" action={{route('developer.additionmyprofile')}}>
                    @csrf
                    @foreach($users as $user)
                        @if(Auth::user()->id == $user->id)
                            <div class="form-group">
                                <label class="col-lg-8 control-label">User Id:<h5> {{$user->id}}</h5></label>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-8 control-label">User Name:<h5> {{$user->name}}</h5></label>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-8 control-label">My Email:<h5> {{$user->email}}</h5></label>
                            </div>
                        @endif
                    @endforeach
                    @foreach($user_info as $user_inf)
                        @if($user_inf->user_id == Auth::user()->id)
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="General info (max 5000 words)">General
                                    info:</label>
                                <div class="col-lg-8">
                            <textarea class="form-control" rows="10" id="General info (max 5000 words)"
                                      name="General info">{{$user_inf->general_info}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label"
                                       for="Education (max 1000 words)">Education:</label>
                                <div class="col-lg-8">
                            <textarea class="form-control" rows="10" id="Education (max 1000 words)"
                                      name="Education">{{$user_inf->education}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label"
                                       for="Experience (max 1000 words)">Experience:</label>
                                <div class="col-lg-8">
                            <textarea class="form-control" rows="10" id="Experience (max 1000 words)"
                                      name="Experience">{{$user_inf->experience}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="Companies where he worked (max 1000 words)">Companies
                                    where he worked:</label>
                                <div class="col-lg-8">
                            <textarea class="form-control" rows="10" id="Companies where he worked (max 1000 words)"
                                      name="Companies where he worked">{{$user_inf->companies_he_worked}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button class="btn btn-success" type="submit"
                                            class="glyphicon glyphicon-thumbs-up"
                                    >Save changes
                                    </button>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-4">
                            <a href="/developer/myprofile/change" class="btn btn-success"><span
                                    class="glyphicon glyphicon-thumbs-up"></span>
                                Enter my profile data</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <hr>

@endsection




