@extends('layouts.developer')

@section('content')
    <div class="container">
        <div class="container h-auto">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="col-lg-10 align-self-end">
                    <h1 class="text-uppercase text-black font-weight-bold">My technologies</h1>
                    <hr class="divider my-4"/>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <form method="PUT" action={{route('developer.changetechnologies')}}>
                @csrf
                <div class="card" style="width: 18rem;">
                    <div class="card-header">
                        My technologies:
                    </div>
                    @foreach($user_technologies as $user_technology)
                        @if(Auth::user()->id == $user_technology->user_id)
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">{{substr($user_technology->technologies->pluck('technology_name'), 2, -2)}}</li>
                            </ul>
                            <input
                                class="form-check-input" type="hidden" name="technology_id[]"
                                value="{{ $user_technology->technology_id }}">
                            @else
                        @endif
                    @endforeach
                </div>
                <div class=btn>
                    <input type="submit" class="btn btn-secondary "
                           name="id" value="change technology">
                </div>
            </form>
        </div>
    </div>
@endsection
