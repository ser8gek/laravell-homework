@extends('layouts.developer')

@section('content')
    <div class="container">
        <div class="container h-auto">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="col-lg-10 align-self-end">
                    <h1 class="text-uppercase text-black font-weight-bold">technologies</h1>
                    <hr class="divider my-4"/>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <form method="GET" action={{route('developer.additiontechnologies')}}>
                @csrf
                <div class="card" style="width: 18rem;">
                    <div class="card-header">
                        Technology:
                    </div>
                    @php $request = request('technology_id')
                    @endphp
                    @foreach($technologies as $technology)
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{ $technology->technology_name }}</li>
                            <div class="form-check">
                                <label class="form-check-label" for="technology_id[]"></label>
                                <input
                                    class="form-check-input" type="checkbox" id="technology_id[]" name="technology_id[]"
                                    value="{{ $technology->id }}"
                                    @if(!empty($request))
                                        @foreach($request as $request_id)
                                            @if ($request_id == $technology->id)checked/>
                                            @endif
                                        @endforeach
                                    @endif
                            </div>
                        </ul>
                    @endforeach
                    <div class="row">
                        <div class="col-md-4 center-block"></div>
                        <div class="col-md-4 ">
                            <button type="submit"
                                    class="btn btn-secondary order-button"
                                    name="technology_id[]">Apply
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
