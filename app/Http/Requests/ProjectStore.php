<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_name' => ['required', 'string', 'max:255'],
            'project_info' => ['required', 'string', 'max:255'],
            'start_date' => ['required', 'date'],
            'finish_date' => ['required', 'date'],
            'developers_on_project' => ['required', 'integer', 'max:11'],
        ];
    }
}
