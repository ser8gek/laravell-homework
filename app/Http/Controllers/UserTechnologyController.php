<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserTechnologyService;
use App\Services\TechnologyService;
use Illuminate\Support\Facades\Auth;
use App\Models\UserTechnology;


class UserTechnologyController extends Controller
{
    public function __construct(UserTechnologyService $user_technologies, TechnologyService $technologies)
    {
        $this->user_technology = $user_technologies;
        $this->technology = $technologies;

    }

    public function index()
    {
        return view('usertechnology.index',
            ["user_technologies" => $this->user_technology->all(),
                'technologies' => $this->technology->all()
            ]);
    }

    public function show(Request $request)
    {

        return view('usertechnology.show',
            ["user_technologies" => $this->user_technology->all(),
                'technologies' => $this->technology->all()
            ]);
    }

    public function store(Request $request)
    {
        $user = Auth::user()->id;
        UserTechnology::where('user_id', $user)->delete();
        $new_technology = new UserTechnology;
        $new_technology->user_id = Auth::user()->id;
        $new_technology->technology_id = $request->technology_id;
        foreach ($request->technology_id as $request_id)
            if ($request_id !== null)
                $new_technology->create(['user_id' => Auth::user()->id,
                    'technology_id' => $request_id]);

        return view('usertechnology.store',
            ["user_technologies" => $this->user_technology->all()
            ]);
    }

}
