<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserInfoService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use App\Models\UserInfo;


class UserInfoController extends Controller
{
    public function __construct(UserInfoService $user_info, UserService $users)
    {
        $this->user_info = $user_info;
        $this->users = $users;

    }

    public function index()
    {

        return view('userinfo.index',
            ["user_info" => $this->user_info->all(), "users" => $this->users->all()]);
    }

    public function show()
    {

        return view('userinfo.show',
            ["user_info" => $this->user_info->all(), "users" => $this->users->all()]);
    }

    public function store(Request $request)
    {
        $user = Auth::user()->id;
        UserInfo::where('user_id', $user)->delete();
        $new_info = new UserInfo;
        $new_info->user_id = Auth::user()->id;
        $new_info->general_info = $request->General_info;
        $new_info->education = $request->Education;
        $new_info->experience = $request->Experience;
        $new_info->companies_he_worked = $request->Companies_where_he_worked;
        $new_info->create(['user_id' => Auth::user()->id, 'general_info' => $request->General_info,
            'education' => $request->Education, 'experience' => $request->Experience,
            'companies_he_worked' => $request->Companies_where_he_worked]);

        return view('userinfo.store',
            ["user_info" => $this->user_info->all(), "users" => $this->users->all()]);
    }

}
