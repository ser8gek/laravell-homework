<?php

namespace App\Http\Controllers;

use App\Services\ProjectService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Project;

class AdminController extends Controller
{
    public function __construct(ProjectService $projects)
    {
        $this->projects = $projects;
    }

    public function index()
    {
        return view('admin.index', ['projects' => $this->projects->all()]);
    }

    public function show(Request $request)
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function destroy(Request $request)
    {
        //
    }
}
