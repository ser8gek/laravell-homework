<?php

namespace App\Http\Controllers;

use App\Services\ProjectService;
use App\Services\TechnologyService;
use App\Services\ProjectTechnologyService;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\ProjectTechnology;
use App\Http\Requests\ProjectStore;
use App\Http\Requests\ProjectEdit;

class AdminProjectController extends Controller
{
    public function __construct(ProjectService $projects, TechnologyService $technologies,
                                ProjectTechnology $project_technology)
    {
        $this->projects = $projects;
        $this->technologies = $technologies;
        $this->project_technology = $project_technology;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_project.index', ['projects' => $this->projects->all(),
                                            'technologies' => $this->technologies->all(),
                                           ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_project.create', ['technologies' => $this->technologies->all(),
            'project_technology' => $this->project_technology->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectStore $request)
    {
        $this->projects->create($request->all());
        return redirect()->route("projects.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project,
                         ProjectTechnologyService $project_technology,
                         TechnologyService $technologies)
    {
        return view('admin_project.show', ['projects' => $project,
            'project_technology' => $project_technology,
            'technologies' => $this->technologies->all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Project $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, TechnologyService $technologies, ProjectTechnologyService $project_technology)
    {

        return view('admin_project.edit', ['projects' => $project, 'technologies' => $technologies->all(), 'project_technology' => $project_technology->all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\ProjectEdit  $request
     * @param  Project $project
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectEdit $request, Project $project)
    {
        $this->projects->update($project->id, $request->all());
        return redirect()->route("projects.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect()->route("projects.index");
    }
}
