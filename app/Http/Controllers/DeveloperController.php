<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProjectService;
use App\Services\UserProjectService;
use App\Services\ProjectTechnologyService;
use App\Services\TechnologyService;



class DeveloperController extends Controller
{
    public function __construct(ProjectService $projects,
                                UserProjectService $user_projects,
                                ProjectTechnologyService $project_technology,
                                TechnologyService $technology)
    {
        $this->projects = $projects;
        $this->user_projects = $user_projects;
        $this->project_technology = $project_technology;
        $this->technology = $technology;
    }

    public function index()
    {
        return view('developer.index',
            ["user_projects" => $this->user_projects->all(), 'projects' => $this->projects->all()]);
    }

    public function show(Request $request)
    {
        $id = $request->all();
        return view('developer.show',
            ["user_projects" => $this->user_projects->all(),
                'projects' => $this->projects->all(),
                'projects_technologies' => $this->project_technology->all(),
                'technologies' => $this->technology->all()]);
    }
}
