<?php


namespace App\Repositories;

use App\Models\ProjectTechnology;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class ProjectTechnologyRepository extends BaseRepository
{
    public function __construct(ProjectTechnology $model)
    {
        $this->model = $model;
    }
}

