<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\UserProject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class UserProjectRepository extends BaseRepository
{

    public function __construct(UserProject $model)
    {
        $this->model = $model;
    }

}
