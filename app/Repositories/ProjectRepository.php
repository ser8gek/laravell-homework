<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Project;
use Illuminate\Database\Eloquent\Model;

class ProjectRepository extends BaseRepository
{

    public function __construct(Project $model)
    {
        $this->model = $model;
    }
}
