<?php


namespace App\Repositories;

use App\Models\UserTechnology;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class UserTechnologyRepository extends BaseRepository
{

    public function __construct(UserTechnology $model)
    {
        $this->model = $model;
    }
}
