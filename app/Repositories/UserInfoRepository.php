<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\UserInfo;
use Illuminate\Database\Eloquent\Model;

class UserInfoRepository extends BaseRepository
{

    public function __construct(UserInfo $model)
    {
        $this->model = $model;
    }
}
