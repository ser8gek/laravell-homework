<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Technology;
use Illuminate\Database\Eloquent\Model;

class TechnologyRepository extends BaseRepository
{

    public function __construct(Technology $model)
    {
        $this->model = $model;
    }
}
