<?php


namespace App\Services;

use App\Repositories\UserInfoRepository;
use App\Services\BaseService;
Use Illuminate\Support\Collection;


class UserInfoService extends BaseService
{
    public function __construct(UserInfoRepository $repo)
    {
        $this->repo = $repo;
    }
}
