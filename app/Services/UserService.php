<?php


namespace App\Services;

use App\Repositories\UserRepository;
use App\Services\BaseService;
Use Illuminate\Support\Collection;


class UserService extends BaseService
{
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }
}
