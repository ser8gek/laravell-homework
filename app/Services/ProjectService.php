<?php


namespace App\Services;

use App\Repositories\ProjectRepository;
use App\Services\BaseService;
Use Illuminate\Support\Collection;


class ProjectService extends BaseService
{
    public function __construct(ProjectRepository $repo)
    {
        $this->repo = $repo;
    }
}
