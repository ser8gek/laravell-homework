<?php


namespace App\Services;

use App\Repositories\ProjectTechnologyRepository;
use App\Services\BaseService;
Use Illuminate\Support\Collection;


class ProjectTechnologyService extends BaseService
{
    public function __construct(ProjectTechnologyRepository $repo)
    {
        $this->repo = $repo;
    }
}
