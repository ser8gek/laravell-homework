<?php


namespace App\Services;

use App\Models\Technology;
use App\Repositories\TechnologyRepository;
use App\Services\BaseService;
Use Illuminate\Support\Collection;


class TechnologyService extends BaseService
{
    public function __construct(TechnologyRepository $repo)
    {
        $this->repo = $repo;
    }

}
