<?php


namespace App\Services;

use App\Repositories\UserTechnologyRepository;
use App\Services\BaseService;
Use Illuminate\Support\Collection;


class UserTechnologyService extends BaseService
{
    public function __construct(UserTechnologyRepository $repo)
    {
        $this->repo = $repo;
    }

}
