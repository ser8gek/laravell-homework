<?php


namespace App\Services;

use App\Repositories\UserProjectRepository;
use App\Services\BaseService;
Use Illuminate\Support\Collection;
use App\Models\UserProject;


class UserProjectService extends BaseService
{
    public function __construct(UserProjectRepository $repo)
    {
        $this->repo = $repo;
    }

}
