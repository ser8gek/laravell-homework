<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $fillable = [
        'id', 'project_name', 'project_info', 'start_date', 'finish_date', 'developers_on_project'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
