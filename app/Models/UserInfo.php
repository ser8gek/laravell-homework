<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $table = 'user_info';

    protected $fillable = [
        'general_info', 'education', 'experience', 'companies_he_worked','user_id', 'image'
    ];
}
