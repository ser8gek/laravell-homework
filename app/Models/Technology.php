<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Technology extends Model
{
    protected $table = 'technologies';

    protected $fillable = [
        'id', 'technology_name'
    ];

    public function usertechnology()
    {
        return $this->belongsTo('App\Models\UserTechnology');
    }
}
