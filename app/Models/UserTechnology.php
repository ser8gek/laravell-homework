<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTechnology extends Model
{
    protected $table = 'user_technologies';

    protected $fillable = [
       'user_id', 'technology_id'
    ];

    public function technologies()
    {
        return $this->hasMany('App\Models\Technology', 'id', 'technology_id');
    }
}
