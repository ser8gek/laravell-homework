<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectTechnology extends Model
{
    protected $table = 'project_technology';

    protected $fillable = [
        'id', 'project_id', 'technology_id'
    ];
    public function technology()
    {
        return $this->hasMany('App\Models\Technology', 'id', 'technology_id');
    }

    protected $hidden = [

    ];
}
