<?php

namespace App\Console\Commands;

use Illuminate\Auth\Events\Validated;
use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AddAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:admin {name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');
        $email = $this->argument('email');
        $password = $this->argument('password');

        $validator = Validator::make(
            [
                'email' => $email,
            ],
            [
                'email' => ['required', 'email', 'unique:users,email']
            ]
        );

        if ($validator->fails()) {
            $this->info('Admin User not created. See error message below:');

            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return 1;
        }

        User::create(
            [
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password),
                'type' => 'admin',
            ]
        );
        $this->info('Thank you ' . $name . 'was registered.');
    }
}
