<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::middleware(['check.role:admin'])->group(function () {
        Route::prefix('admin')->group(function () {
            Route::get('/', 'AdminController@index')->name('admin.home');
            Route::resource('projects', 'AdminProjectController');
            Route::resource('technologies', 'AdminTechnologyController');
            Route::resource('users', 'AdminUserController');
        });
    });
    Route::middleware(['check.role:developer'])->group(function () {
        Route::get('/developer', 'DeveloperController@index')->name('developer.home');
        Route::post('/developer/project', 'DeveloperController@show')->name('developer.project');
        Route::get('/developer/mytechnologies', 'UserTechnologyController@index')->name('developer.mytechnologies');
        Route::get('/developer/mytechnologies/change', 'UserTechnologyController@show')->name('developer.changetechnologies');
        Route::get('/developer/mytechnologies/addition', 'UserTechnologyController@store')->name('developer.additiontechnologies');
        Route::get('/developer/myprofile', 'UserInfoController@index')->name('developer.myprofile');
        Route::get('/developer/myprofile/change', 'UserInfoController@show')->name('developer.changemyprofile');
        Route::get('/developer/myprofile/addition', 'UserInfoController@store')->name('developer.additionmyprofile');
    });
});


